import Campaign from './campaign'

export default interface Campaigns {
  count: number;
  campaigns: Array<Campaign>;
}
