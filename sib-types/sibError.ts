export default interface SibError {
  code: string;
  message: string;
}
