export default interface Campaign {
  id: number;
  name: string;
  type: string;
  status: string;
  testSent: boolean;
  header: string;
  footer: string;
  sender: any;
  replyTo: string;
  toField: string;
  htmlContent: string;
  tag: string;
  inlineImageActivation: boolean;
  mirrorActive: boolean;
  recipients: any;
  statistics: any;
  subject: string;
  scheduledAt: string;
  createdAt: string;
  modifiedAt: string;
  shareLink: string;
  sendAtBestTime: boolean;
  abTesting: boolean;
}
