import dayjs from 'dayjs'
import * as dotenv from 'dotenv'
import nodeFetch, { Response } from 'node-fetch'
import Campaign from './sib-types/campaign'
import Campaigns from './sib-types/campaigns'
import SibError from './sib-types/sibError'

dotenv.config()
if (!process.env.SENDINBLUE_API_KEY)
  throw Error('SENDINBLUE_API_KEY has to be defined in .env file.')

const SENDINBLUE_API_KEY = process.env.SENDINBLUE_API_KEY

getSuspendedCampaigns().then((suspendedCampaigns: Array<Campaign>) => {
  suspendedCampaigns.forEach(async (suspendedCampaign: Campaign) => {
    await sendCampaign(suspendedCampaign.id)
  })
})

async function getSuspendedCampaigns(): Promise<Array<Campaign>> {
  const url = urlWithQueryStrings(
    'https://api.sendinblue.com/v3/emailCampaigns',
    { status: 'suspended', limit: '100', offset: '0' }
  )

  consoleLogWithDate('Getting campaigns...')

  const res: Response = await nodeFetch(url, {
    headers: {
      accept: 'application/json',
      'api-key': SENDINBLUE_API_KEY
    }
  })

  const body: Campaigns = await res.json()

  if (!body.campaigns) body.campaigns = []
  if (!body.count) body.count = 0

  consoleLogWithDate(
    `Got ${body.campaigns.length} suspended campaigns (total suspended campaigns: ${body.count}):`
  )

  body.campaigns.forEach((suspendedCampaign: Campaign) => {
    consoleLogWithDate(
      `Campaign ${suspendedCampaign.id}: ${suspendedCampaign.name}`
    )
  })

  return body.campaigns
}

async function sendCampaign(campaignId: number): Promise<void> {
  consoleLogWithDate(`Sending campaign ${campaignId}`)

  const res: Response = await nodeFetch(
    `https://api.sendinblue.com/v3/emailCampaigns/${campaignId}/sendNow`,
    {
      method: 'POST',
      headers: {
        accept: 'application/json',
        'api-key': SENDINBLUE_API_KEY
      }
    }
  )

  if (res.ok) {
    consoleLogWithDate(`Campaign ${campaignId} successfully scheduled !`)
    return
  }

  const body: SibError = await res.json()

  switch (res.status) {
    case 400:
      consoleLogWithDate(
        `Campaign ${campaignId} could not be sent: Error ${body.code}, ${body.message}`
      )
      return
    case 402:
      consoleLogWithDate(
        `You don't have enough credit to send your campaign ${campaignId}. Please update your plan: Error ${body.code}, ${body.message}`
      )
      return
    case 404:
      consoleLogWithDate(
        `Campaign ${campaignId} not found: Error ${body.code}, ${body.message}`
      )
  }
}

function urlWithQueryStrings(
  urlString: string,
  queryStrings: { [key: string]: string }
): URL {
  const url: URL = new URL(urlString)

  Object.keys(queryStrings).forEach(key =>
    url.searchParams.append(key, queryStrings[key])
  )

  return url
}

function consoleLogWithDate(message: string): void {
  console.log(`${dayjs().format('DD/MM/YYYY HH:mm:ss')}: ${message}`)
}
