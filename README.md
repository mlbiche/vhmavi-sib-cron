# vhmavi-sib-cron

This tiny utils restart every days SendinBlue campaigns for the association ASVHMAVI Nepal.

## ⛓ Dependencies

To run _La prochaine tournée_, you need:

- `node.js` _([download link](https://nodejs.org/en/))_. After the installation, check that `node` is properly installed by running in the terminal of your choice `node -v`. The command should display the `node` installed version.
- `pnpm` _([installation guide](https://pnpm.io/installation))_, a performant alternative to `npm`. You can also check it installed successfully by running `pnpm -v`.

And that's it! The project will install all the node.js module dependencies itself on the next step. 🤙

## 🏃 Quick start

In the terminal of your choice, go through the following steps:

- Clone the project to the folder of your choice.
- Install the project: `pnpm install` _→ It installs all the needed node.js modules._

  **Note:** You only have to run this command the first time.

- Build the project: `pnpm run build`
- Start the project: `pnpm start` _→ It launches the script._

## 🚀 Deploying

On your server:

- Clone the repository in the _/srv/_ folder
- Install the project: `pnpm install`
- Build the project: `pnpm run build`
- Set the daily cron: `pnpm run cron-setup`
